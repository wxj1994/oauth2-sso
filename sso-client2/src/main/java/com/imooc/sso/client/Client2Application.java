package com.imooc.sso.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

/**
* Created by WangXJ
* 2019-05-05 16:38
*/
@SpringBootApplication
@EnableOAuth2Sso
public class Client2Application {

	public static void main(String[] args) throws Exception {
		
		SpringApplication.run(Client2Application.class, args);
	}

}
