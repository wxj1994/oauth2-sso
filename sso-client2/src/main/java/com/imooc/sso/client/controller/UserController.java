package com.imooc.sso.client.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Created by WangXJ
* 2019-05-05 16:39
*/
@RestController
public class UserController {

	@GetMapping("/getUser")
	public Authentication getUser(Authentication user) {
		
		return user;
	}
}
