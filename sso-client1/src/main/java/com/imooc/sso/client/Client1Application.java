package com.imooc.sso.client;
/**
* Created by WangXJ
* 2019-05-05 15:45
*/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@SpringBootApplication
@EnableOAuth2Sso
public class Client1Application {

	public static void main(String[] args) throws Exception {
		
		SpringApplication.run(Client1Application.class, args);
	}

}
