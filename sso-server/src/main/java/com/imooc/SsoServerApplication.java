package com.imooc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
* Created by WangXJ
* 2019-05-05 14:12
*/
@SpringBootApplication
public class SsoServerApplication {

	public static void main(String[] args) throws Exception {
		
		SpringApplication.run(SsoServerApplication.class, args);
	}

}
