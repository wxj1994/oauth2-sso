package com.imooc.sso.server.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.imooc.sso.server.security.MyUserDetailsService;

/**
* Created by WangXJ
* 2019-04-28 14:45
*/
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{

	@Resource
	private MyUserDetailsService myUserDetailsService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable();
		http.formLogin()
			.and()
			.authorizeRequests()
				.anyRequest().authenticated();
		
		
//		http.requestMatchers()
//				.antMatchers("/oauth/**")
//				.antMatchers("/login/**")
//				.antMatchers("/logout/**")
//				.and()
		
//		http.authorizeRequests()
//				.antMatchers("/oauth/**").authenticated()
//				.and()
//			.formLogin().permitAll();		
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.userDetailsService(myUserDetailsService)
			.passwordEncoder(passwordEncoder());
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
