package com.imooc.sso.server.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
* Created by WangXJ
* 2019-05-05 14:17
*/
@Configuration
@EnableAuthorizationServer 
public class SsoAuthorizationServerConfig extends AuthorizationServerConfigurerAdapter{
	
	@Resource
	@Lazy
	private PasswordEncoder passwordEncoder;

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		
		clients.inMemory()
				.withClient("imooc1")
//				.secret("{noop}imoocsecret1")
				.secret(passwordEncoder.encode("imoocsecret1"))
				.authorizedGrantTypes("authorization_code", "refresh_token")
				.redirectUris("http://127.0.0.1:8080/client1/login")
				.scopes("all")
				.autoApprove(true)// 开启-自动授权
				.and()
				.withClient("imooc2")
//				.secret("{noop}imoocsecret2")
				.secret(passwordEncoder.encode("imoocsecret2"))
				.authorizedGrantTypes("authorization_code", "refresh_token")
				.redirectUris("http://127.0.0.1:8060/client2/login")
				.scopes("all")
				.autoApprove(true);// 开启-自动授权
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		
		endpoints.tokenStore(jwtTokenStore())
				 .accessTokenConverter(jwtAccessTokenConverter());
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		
		security.tokenKeyAccess("isAuthenticated()");
	}

	@Bean
	public TokenStore jwtTokenStore() {
		return new JwtTokenStore(jwtAccessTokenConverter());
	}

	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey("imooc");// token key
		return converter;
	}

	
}
