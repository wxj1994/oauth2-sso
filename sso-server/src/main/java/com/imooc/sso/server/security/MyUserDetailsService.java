package com.imooc.sso.server.security;

import javax.annotation.Resource;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
* Created by WangXJ
* 2019-05-10 11:13
*/
@Component
public class MyUserDetailsService implements UserDetailsService {
	
	@Resource
	private PasswordEncoder passwordEncoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		return new User(username, passwordEncoder.encode("123456"), 
				AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));
	}

}
